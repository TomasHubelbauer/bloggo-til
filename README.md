# Today I Learned

## 2018-03-14

Running TypeScript on Dokku is best achieved by compiling TypeScript to JavaScript
in `start` and then running the JavaScript as usual.

More Dokku tips:

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-dokku/blob/master/README.md#tips)
- [Bloggo post](http://hubelbauer.net/post/bloggo-til)

## 2018-03-08

GitHub flavored MarkDown will render `<kbd>` in an actual keyboard key style.

This is kind of a duh thing, but I legit didn't make the connection.

## 2018-03-08

To disable pushing a Git repository to a remote, this command can be used:

`git remote set-url --push origin no_push`

Pulling continues to work, see `git remove -v` for the fetch URL being left intact.

## 2018-02-22

You can't trap some browser shortcuts like `Ctrl+N`, `Ctrl+Shift+N` etc.

Even when `preventDefault` is called and `false` returned, the browser will
still process them. This is presumably to prevent scummy apps from faking new
tabs and capturing sensitive data and whatnot.

You can trap some less risky shortcuts, like *Find*, *Print*, *Bookmark*, etc.

## 2018-02-21

In JavaScript, the `mousemove` (and `pointermove`) events always have `button`
set to `0` (as it is not a button action that triggers the event, but movement).

To query the state of the button the the `move` events, `buttons` must be used.

## 2018-02-20

There is a `relativeTarget` field on browser events, which is useful for when
one wants to dismiss a form when its field lose focus (like a popup editor),
but not when the focus was moved to another field in the form.

In that case one would say:

```javascript
editorInput.addEventListener('blur', ({ relatedTarget }) => {
    if (!relatedTarget || relatedTarget.parentNode !== editorForm) {
        dismissEditor();
    } 
});
```
